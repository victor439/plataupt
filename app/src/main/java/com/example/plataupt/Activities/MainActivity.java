package com.example.plataupt.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.plataupt.R;

public class MainActivity extends AppCompatActivity {
private TextView CrearCuenta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CrearCuenta = findViewById(R.id.textViewCrearCuenta);
        CrearCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent crearcuenta = new Intent(MainActivity.this,Registro.class);
                startActivity(crearcuenta);
            }
        });
    }
}
