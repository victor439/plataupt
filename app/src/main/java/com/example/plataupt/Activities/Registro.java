package com.example.plataupt.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.plataupt.Api.Api;
import com.example.plataupt.Api.Servicios.serviciopeticion;
import com.example.plataupt.Models.viewModelss.Registro_Usuario;
import com.example.plataupt.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


//Victor Manuel Gonzalez Ugalde
public class Registro extends AppCompatActivity {
private TextView RegresarLogin;
private EditText usuario;
private EditText contra;
private EditText Recontra;
private Button Registrar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        RegresarLogin = findViewById(R.id.textRegresar);
        usuario = findViewById(R.id.editTextUsu);
        contra =findViewById(R.id.editTextCon);
        Recontra = findViewById(R.id.editTextReCon);
        Registrar = findViewById(R.id.buttonRegistrar);
        RegresarLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Registro.this,MainActivity.class);
                startActivity(intent);
            }
        });

        Registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.equals(contra.getText(), Recontra.getText()))
                    Recontra.setError("La contraseña no coincide");
                else {
                    serviciopeticion service = Api.getApi(Registro.this).create(serviciopeticion.class);
                    Call<Registro_Usuario> registro_usuarioCall = service.registrarUsuario(usuario.getText().toString(), contra.getText().toString());
                    registro_usuarioCall.enqueue(new Callback<Registro_Usuario>() {
                        @Override
                        public void onResponse(Call<Registro_Usuario> call, Response<Registro_Usuario> response) {
                            Registro_Usuario peticion = response.body();
                            if (response.body() == null) {
                                Toast.makeText(Registro.this, "Hay un error inesperado", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            if (peticion.estado == "true") {
                                startActivity(new Intent(Registro.this, MainActivity.class));
                                Toast.makeText(Registro.this, "El registro de datos es Exitoso", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(Registro.this, peticion.detalle, Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onFailure(Call<Registro_Usuario> call, Throwable t) {
                            Toast.makeText(Registro.this, "No hay conexión[Error] ", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }
}
